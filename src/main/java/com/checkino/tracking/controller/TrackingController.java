package com.checkino.tracking.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.checkino.tracking.domain.Tracking;
import com.checkino.tracking.repository.TrackingRepository;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKTReader;

@RestController
@RequestMapping("/api/tracking")
public class TrackingController {

	@Autowired
	private TrackingRepository trackingRepository;

	@PostMapping("/save")
	public Tracking createTracking(@Valid @RequestBody Tracking tracking) throws ParseException {
		WKTReader fromText = new WKTReader();
		Long longitude = tracking.getLongitude();
		Long latitude = tracking.getLatitude();
		Geometry geom = fromText.read("POINT("+ longitude + " " +  latitude +")");
		tracking.setGeom(geom);
		return trackingRepository.save(tracking);
	}

	@GetMapping("/get")
	public List<Tracking> getTracking() throws ParseException {
		WKTReader fromText = new WKTReader();
		Geometry geom = null;
		geom = fromText.read("POLYGON((-107 30, -102 30, -102 41, -107 41, -107 30))");
		return trackingRepository.findWithin(geom);
	}

}
