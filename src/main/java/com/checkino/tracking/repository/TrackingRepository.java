package com.checkino.tracking.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.checkino.tracking.domain.Tracking;
import com.vividsolutions.jts.geom.Geometry;

public interface TrackingRepository extends CrudRepository<Tracking,Long>{
	
	@Query("select c from Tracking c where within(c.geom, ?1) = true")
    List<Tracking> findWithin(Geometry geom);
	
	
	

}
